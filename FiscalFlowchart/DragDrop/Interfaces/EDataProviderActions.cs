using System;

namespace FiscalFlowchart.DragDrop.Interfaces
{
    [Flags]
    public enum EDataProviderActions
    {
        QueryContinueDrag = 0x01, // Call IDataProvider.DragSource_QueryContinueDrag
        GiveFeedback = 0x02, // Call IDataProvider.DragSource_GiveFeedback
        DoDragDropDone = 0x04, // Call IDataProvider.DoDragDropDone

        None = 0x00,
    }
}