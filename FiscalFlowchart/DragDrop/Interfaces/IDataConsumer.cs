using System.Windows;

namespace FiscalFlowchart.DragDrop.Interfaces
{
    /// <summary>
    /// A declaration of actions that can be performed on dragged data
    /// </summary>
    public interface IDataConsumer
    {
        EDataConsumerActions EDataConsumerActions { get; }

        void DropTarget_DragEnter( object sender, DragEventArgs e );
        void DropTarget_DragOver( object sender, DragEventArgs e );
        void DropTarget_Drop( object sender, DragEventArgs e );
        void DropTarget_DragLeave( object sender, DragEventArgs e );
    }
}