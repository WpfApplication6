using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

using FiscalFlowchart.DragDrop.Interfaces;
using FiscalFlowchart.IEnumerableExtras;

using WpfApplication6;

namespace FiscalFlowchart.DragDrop.Workers
{
    /// <summary>
    /// This data consumer looks for ListBoxItems.
    /// The ListBoxItem is inserted before the
    /// target ListBoxItem or at the end of the
    /// list if dropped on empty space.
    /// </summary>
    /// <typeparam name="TContainer">Drag source and drop destination container type</typeparam>
    /// <typeparam name="TObject">Drag source and drop destination object type</typeparam>
    public class ListBoxDataConsumer< TContainer, TObject > : DataConsumerBase, IDataConsumer
        where TContainer : ItemsControl
        where TObject : ListBoxItem
    {
        public ListBoxDataConsumer( string[] dataFormats )
            : base( dataFormats )
        {
        }

        public override EDataConsumerActions EDataConsumerActions
        {
            get
            {
                return
                    EDataConsumerActions.DragEnter |
                    EDataConsumerActions.DragOver |
                    EDataConsumerActions.Drop |
                    //DragDropDataConsumerActions.DragLeave |
                    EDataConsumerActions.None;
            }
        }

        public override void DropTarget_DragEnter( object sender, DragEventArgs e )
        {
            DragOverOrDrop( false, sender, e );
        }

        public override void DropTarget_DragOver( object sender, DragEventArgs e )
        {
            DragOverOrDrop( false, sender, e );
        }

        public override void DropTarget_Drop( object sender, DragEventArgs e )
        {
            DragOverOrDrop( true, sender, e );
        }

        /// <summary>
        /// First determine whether the drag data is supported.
        /// Finally handle the actual drop when <code>bDrop</code> is true.
        /// Insert the item before the drop target.  When there is no drop
        /// target (dropped on empty space), add to the end of the items.
        /// </summary>
        /// <param name="bDrop">True to perform an actual drop, otherwise just return e.Effects</param>
        /// <param name="sender">DragDrop event <code>sender</code></param>
        /// <param name="e">DragDrop event arguments</param>
        private void DragOverOrDrop( bool bDrop, object sender, DragEventArgs e )
        {
            ListBoxDataProvider<TContainer, TObject> dataProvider = GetData( e ) as ListBoxDataProvider<TContainer, TObject>;
            if ( dataProvider != null )
            {
                TContainer dragSourceContainer = dataProvider.SourceContainer as TContainer;
                TObject dragSourceObject = dataProvider.SourceObject as TObject;
                Debug.Assert( dragSourceObject != null );
                Debug.Assert( dragSourceContainer != null );

                TContainer dropContainer = Utilities.FindParentControlIncludingMe<TContainer>( e.Source as DependencyObject );
                TObject dropTarget = e.Source as TObject;

                if ( dropContainer != null )
                {
                    if ( bDrop && dropTarget != dragSourceObject )
                    {
                        dataProvider.Unparent();

                        if ( dropTarget == null )
                        {
                            dropTarget = dropContainer.Items
                                .Reduce<TObject>( ( a, b ) =>
                                                      {
                                                          var p1 = e.GetPosition( a );
                                                          var p2 = e.GetPosition( b );

                                                          var d1 = Math.Abs( p1.X ) + Math.Abs( p1.Y );
                                                          var d2 = Math.Abs( p2.X ) + Math.Abs( p2.Y );

                                                          return d1 < d2 ? a : b;
                                                      } );

                            if ( dropTarget != null )
                            {
                                var p1 = e.GetPosition( dropTarget );

                                var x = Math.Abs( p1.X );
                                var y = Math.Abs( p1.Y );

                                if ( x > dropTarget.ActualWidth || y > dropTarget.ActualHeight )
                                {
                                    dropTarget = null;
                                }
                            }

                            if ( dropTarget != null )
                            {
                                dropContainer.Items.Insert( dropContainer.Items.IndexOf( dropTarget ), dragSourceObject );
                            }
                            else
                            {
                                dropContainer.Items.Add( dragSourceObject );
                            }
                        }
                        else
                        {
                            if ( dropTarget != dragSourceObject )
                            {
                                dropContainer.Items.Insert( dropContainer.Items.IndexOf( dropTarget ), dragSourceObject );
                            }
                        }

                        dragSourceObject.IsSelected = true;
                        dragSourceObject.BringIntoView();
                    }
                    e.Effects = DragDropEffects.Move;
                    e.Handled = true;
                }
                else
                {
                    e.Effects = DragDropEffects.None;
                    e.Handled = true;
                }
            }
        }
    }
}