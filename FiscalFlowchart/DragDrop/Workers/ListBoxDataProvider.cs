using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

using FiscalFlowchart.DragDrop.Interfaces;

namespace FiscalFlowchart.DragDrop.Workers
{
    /// <summary>
    /// This Data Provider represents ListBoxItems.
    /// </summary>
    /// <typeparam name="TContainer">Drag source container type</typeparam>
    /// <typeparam name="TObject">Drag source object type</typeparam>
    public class ListBoxDataProvider< TContainer, TObject > : DataProviderBase<TContainer, TObject>, IDataProvider
        where TContainer : ItemsControl
        where TObject : FrameworkElement
    {
        public ListBoxDataProvider( string dataFormatString ) :
            base( dataFormatString )
        {
        }

        public override DragDropEffects AllowedEffects
        {
            get
            {
                return
                    //DragDropEffects.Copy |
                    //DragDropEffects.Scroll |
                    DragDropEffects.Move | // Move ListBoxItem
                    DragDropEffects.Link | // Needed for moving ListBoxItem as a TreeView sibling
                    DragDropEffects.None;
            }
        }

        public override EDataProviderActions EDataProviderActions
        {
            get
            {
                return
                    EDataProviderActions.QueryContinueDrag | // Need Shift key info (for TreeView)
                    EDataProviderActions.GiveFeedback |
                    //DragDropDataProviderActions.DoDragDropDone |
                    EDataProviderActions.None;
            }
        }

        public override void DragSource_GiveFeedback( object sender, GiveFeedbackEventArgs e )
        {
            if ( e.Effects == DragDropEffects.Move )
            {
                e.UseDefaultCursors = true;
                e.Handled = true;
            }
            else if ( e.Effects == DragDropEffects.Link )
            {
                // ... when Shift key is pressed
                e.UseDefaultCursors = true;
                e.Handled = true;
            }
        }

        public override void Unparent()
        {
            TObject item = SourceObject as TObject;
            TContainer container = SourceContainer as TContainer;

            Debug.Assert( item != null, "Unparent expects a non-null item" );
            Debug.Assert( container != null, "Unparent expects a non-null container" );

            if ( ( container != null ) && ( item != null ) )
            {
                container.Items.Remove( item );
            }
        }

        public override bool AddAdorner
        {
            get
            {
                return true;
            }
        }
    }
}