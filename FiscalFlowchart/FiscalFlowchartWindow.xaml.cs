﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Media3D;

using FiscalFlowchart.DragDrop;
using FiscalFlowchart.DragDrop.Interfaces;
using FiscalFlowchart.DragDrop.Workers;

namespace FiscalFlowchart
{
    /// <summary>
    /// Interaction logic for FiscalFlowchartWindow.xaml
    /// </summary>
    public partial class FiscalFlowchartWindow : Window
    {
        private List<Adorner> _adorners = new List<Adorner>();

        public FiscalFlowchartWindow()
        {
            InitializeComponent();

            // Data Provider
            ListBoxDataProvider<ListBox, ListBoxItem> listBoxDataProvider =
                new ListBoxDataProvider<ListBox, ListBoxItem>( "ListBoxItemObject" );

            // Data Consumer
            ListBoxDataConsumer<ListBox, ListBoxItem> listBoxDataConsumer =
                new ListBoxDataConsumer<ListBox, ListBoxItem>( new[] {"ListBoxItemObject"} );

            // Drag Managers
            DragManager dragHelperListBox1 = new DragManager( listbox1, listBoxDataProvider );
            DragManager dragHelperListBox2 = new DragManager( listbox2, listBoxDataProvider );
            DragManager dragHelperListBox3 = new DragManager( listbox3, listBoxDataProvider );
            DragManager dragHelperListBox4 = new DragManager( listbox4, listBoxDataProvider );
            DragManager dragHelperListBox5 = new DragManager( listbox5, listBoxDataProvider );
            DragManager dragHelperListBox6 = new DragManager( listbox6, listBoxDataProvider );
            DragManager dragHelperListBox7 = new DragManager( listbox7, listBoxDataProvider );

            // Drop Managers
            DropManager dropManager1 = new DropManager( listbox1, new IDataConsumer[] {listBoxDataConsumer} );
            DropManager dropManager2 = new DropManager( listbox2, new IDataConsumer[] {listBoxDataConsumer} );
            DropManager dropManager3 = new DropManager( listbox3, new IDataConsumer[] {listBoxDataConsumer} );
            DropManager dropManager4 = new DropManager( listbox4, new IDataConsumer[] {listBoxDataConsumer} );
            DropManager dropManager5 = new DropManager( listbox5, new IDataConsumer[] {listBoxDataConsumer} );
            DropManager dropManager6 = new DropManager( listbox6, new IDataConsumer[] {listBoxDataConsumer} );
            DropManager dropManager7 = new DropManager( listbox7, new IDataConsumer[] {listBoxDataConsumer} );

            carol.MouseDoubleClick += ListBoxItem_MouseDown;
            PreviewMouseDown += new MouseButtonEventHandler( FiscalFlowchartWindow_MouseDown );
        }

        private void FiscalFlowchartWindow_MouseDown( object sender, MouseButtonEventArgs e )
        {
            Visual visual = Application.Current.MainWindow.Content as Visual;
            var layer = AdornerLayer.GetAdornerLayer( visual );

            if ( !( e.OriginalSource as DependencyObject ).Is<ScrollBar>() )
            {
                _adorners.ForEach( layer.Remove );
                _adorners.Clear();
            }
        }

        private void ListBoxItem_MouseDown( object sender, MouseButtonEventArgs e )
        {
            var obj = sender as UIElement;

            var list = new ListBox();
            list.SetOrientation( Orientation.Horizontal );

            var x = FindResource( typeof( ListBoxItem ) ) as Style;
            list.Items.Add( new ListBoxItem {Content = "VAR1", Style = x} );
            list.Items.Add( new ListBoxItem {Content = "VAR2", Style = x} );
            list.Items.Add( new ListBoxItem {Content = "VAR3", Style = x} );
            var item = new ListBoxItem {Content = "VAR4", Style = x};
            item.MouseDown += new MouseButtonEventHandler( item_MouseDoubleClick );
            item.PreviewMouseDown += new MouseButtonEventHandler( item_PreviewMouseDown );
            list.Items.Add( item );

            list.Style = FindResource( typeof( ListBox ) ) as Style;

            var tr = new ScaleTransform( 1, 1 );

            Duration d = new Duration( TimeSpan.FromMilliseconds( 300 ) );
            var anim1 = new DoubleAnimation {From = .1, To = 1, Duration = d, AccelerationRatio = .5, DecelerationRatio = .2};

            tr.BeginAnimation( ScaleTransform.ScaleXProperty, anim1 );
            tr.BeginAnimation( ScaleTransform.ScaleYProperty, anim1 );

            list.RenderTransform = tr;

            Visual visual = Application.Current.MainWindow.Content as Visual;
            var layer = AdornerLayer.GetAdornerLayer( visual );

            var adorner = new PopupAdorner( obj, list );
            layer.PreviewMouseDown += new MouseButtonEventHandler( layer_PreviewMouseDown );
            adorner.PreviewMouseDown += new MouseButtonEventHandler( adorner_PreviewMouseDown );
            layer.Add( adorner );
            _adorners.Add( adorner );
        }

        private void adorner_PreviewMouseDown( object sender, MouseButtonEventArgs e )
        {
            Trace.WriteLine( "Adorner PreviewMouseDown" );
        }

        private void layer_PreviewMouseDown( object sender, MouseButtonEventArgs e )
        {
            Trace.WriteLine( "Layer PreviewMouseDown" );
        }

        private void item_PreviewMouseDown( object sender, MouseButtonEventArgs e )
        {
            throw new NotImplementedException();
        }

        private void adorner_MouseDown( object sender, MouseButtonEventArgs e )
        {
            throw new NotImplementedException();
        }

        private void item_MouseDoubleClick( object sender, MouseButtonEventArgs e )
        {
            throw new NotImplementedException();
        }
    }

    internal class PopupAdorner : Adorner
    {
        private readonly UIElement _obj;
        private readonly ListBox _child;

        public PopupAdorner( UIElement obj, ListBox list ) :
            base( obj )
        {
            _obj = obj;
            _child = list;
        }

        private double X
        {
            get
            {
                return ( _obj.RenderSize.Width - _child.RenderSize.Width ) / 2;
            }
        }

        private double Y
        {
            get
            {
                return _obj.RenderSize.Height * .9;
            }
        }

        protected override int VisualChildrenCount
        {
            get
            {
                return 1;
            }
        }

        protected override Visual GetVisualChild( int index )
        {
            Debug.Assert( index == 0, "Index must be 0, there's only one child" );
            return _child;
        }

        protected override Size MeasureOverride( Size finalSize )
        {
            _child.Measure( finalSize );
            return _child.DesiredSize;
        }

        protected override Size ArrangeOverride( Size finalSize )
        {
            _child.Arrange( new Rect( finalSize ) );
            return finalSize;
        }

        public override GeneralTransform GetDesiredTransform( GeneralTransform transform )
        {
            GeneralTransformGroup newTransform = new GeneralTransformGroup();
            newTransform.Children.Add( base.GetDesiredTransform( transform ) );
            newTransform.Children.Add( new TranslateTransform( X, Y ) );
            return newTransform;
        }
    }

    public static class ListboxOrientation
    {
        public static void SetOrientation( this ListBox list, Orientation orientation )
        {
            FrameworkElementFactory factory =
                new FrameworkElementFactory( typeof( VirtualizingStackPanel ) );

            factory.SetValue( VirtualizingStackPanel.OrientationProperty, orientation );

            list.ItemsPanel = new ItemsPanelTemplate( factory );
        }
    }

    public static class DependencyObjectIs
    {
        public static bool Is< TDependencyObject >( this DependencyObject dependencyObject ) where TDependencyObject : DependencyObject
        {
            var result = false;
            while ( dependencyObject != null )
            {
                if ( dependencyObject is TDependencyObject )
                {
                    result = true;
                    break;
                }

                // VisualTreeHelper works with objects of type Visual or Visual3D.
                // If the current object is not derived from Visual or Visual3D,
                // then use the LogicalTreeHelper to find the parent element.
                if ( dependencyObject is Visual || dependencyObject is Visual3D )
                {
                    dependencyObject = VisualTreeHelper.GetParent( dependencyObject );
                }
                else
                {
                    dependencyObject = LogicalTreeHelper.GetParent( dependencyObject );
                }
            }

            return result;
        }
    }
}